import scrapy
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()
import requests
from scrapy.utils.project import get_project_settings
import zipfile
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from apiclient import errors
from apiclient.http import MediaFileUpload
import datetime
from dateutil.relativedelta import relativedelta
from scrapy.http import Request
import os
from scrapy import log
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import smtplib
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import win32con, win32api
import requests 
#from lxml import html

class quotesspider(scrapy.Spider):
    name = 'quotesspider'
    allowed_domains=['cc.gov.in']
    start_urls = ['https://www.cci.gov.in/what-s-new']
    custom_settings = {
       'FEED_FORMAT' : 'csv',
       'FEED_URI' : 'CCI.csv'
    }
#    download_delay = 1.5
    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        
          
    def spider_closed(self, spider):
            settings = get_project_settings() 

##        working_folder = settings.get('FOLDERPATH')
##        for root, dirs, files in os.walk(working_folder):
##            b=0
##            for file in files:
##                if file.endswith(".doc") or file.endswith(".pdf") or file.endswith(".xls") or file.endswith(".csv") or file.endswith(".zip"): 
##                    a=1
##                else:
##                    b=1
##        print(a)
##        print(b)           
##        if a >= 1 and not b > 1:
            working_folder = settings.get('FOLDERPATH')
          
            files = os.listdir(working_folder)
            
            files_py = []
            
            for f in files:
                if f.endswith('.pdf') or f.endswith('.doc') or f.endswith('.xls'):
                    fff = os.path.join(working_folder, f)
                    files_py.append(fff)
            
            ZipFile = zipfile.ZipFile("CCI.zip", "w")
             
            for a in files_py:
                ZipFile.write(os.path.basename(a), compress_type=zipfile.ZIP_DEFLATED)
            ZipFile.close()
            creds = None
            
            SCOPES = ['https://www.googleapis.com/auth/drive']
            # The file token.pickle stores the user's access and refresh tokens, and is
            # created automatically when the authorization flow completes for the first
            # time.
            if os.path.exists('token.pickle'):
                with open('token.pickle', 'rb') as token:
                    creds = pickle.load(token)
            # If there are no (valid) credentials available, let the user log in.
            if not creds or not creds.valid:
                if creds and creds.expired and creds.refresh_token:
                    creds.refresh(Request())
                else:
                    flow = InstalledAppFlow.from_client_secrets_file(
                        'credentials.json', SCOPES)
                    creds = flow.run_local_server()
                # Save the credentials for the next run
                with open('token.pickle', 'wb') as token:
                    pickle.dump(creds, token)
                    
            service = build('drive', 'v3', credentials=creds, cache_discovery=False)
            TODAYDATESS=datetime.datetime.today() 
            TODAYDATETEST=TODAYDATESS.strftime('%Y-%m-%d')
            file_metadata = {
            'name': 'Scrapy [%s]' % TODAYDATETEST,
            'mimeType': 'application/vnd.google-apps.folder'
            }
            title = 'Scrapy [%s]' % TODAYDATETEST
            results = service.files().list(
                pageSize=10, fields="nextPageToken, files(id, name)").execute()
            items = results.get('files', [])
            a=0
            folder_id = 0
            if not items:
                print('No files found.')
            else:
                print('Files:')
                for item in items:
                    print(u'{0} ({1})'.format(item['name'], item['id']))
                    if item['name'] == title:
                        print("present")
                        a=item['id']                
                        break
                    else:
                        print("Not present")
                        a = 0
            if a==0:
                file = service.files().create(body=file_metadata,
                                                fields='id').execute()
                print('Folder ID: %s' % file.get('id'))
                folder_id = file.get('id')
            else:
                folder_id = a 

            file = service.permissions().create(body={"role":"reader", "type":"anyone"}, fileId=folder_id).execute()
            file_name =['CCI.csv']
            for name in file_name:
                file_metadata = {
                'name': [name] ,
                'parents': [folder_id]
                }
                media = MediaFileUpload(name,
                                    mimetype='text/csv',
                                    resumable=True)
                file = service.files().create(body=file_metadata,
                                                media_body=media,
                                                fields='id').execute()
            file_name =['CCI.zip']
            for name in file_name:
                file_metadata = {
                'name': [name] ,
                'parents': [folder_id]
                }
                media = MediaFileUpload(name,
                                    mimetype='application/zip',
                                    resumable=True)
                file = service.files().create(body=file_metadata,
                                                media_body=media,
                                                fields='id').execute()
                
           
            print ('File ID: %s' % file.get('id'))
            file_name =['CCI.csv','CCI.zip']
            working_folder = settings.get('FOLDERPATH')
            files = os.listdir(working_folder)
            
            for f in files:
                if f.endswith('.pdf') or f.endswith('.xls') or f.endswith('.doc'):
                    win32api.SetFileAttributes(f, win32con.FILE_ATTRIBUTE_NORMAL)
                    os.remove(f)
            for file in file_name:
                    
                    win32api.SetFileAttributes(file, win32con.FILE_ATTRIBUTE_NORMAL)
                    os.remove(file)               
#   
             
    def parse(self,response):
        settings = get_project_settings()
        somedata = response.xpath('//*[@id="block-system-main"]/div/div/div[1]/table/tbody/tr/td[img[@class="file-icon"]]/ancestor::*[position()=1]/td/a/@href').extract()
        if not somedata:
           log.msg("This should never happen, XPath's are all wrong, OMG!", level=log.CRITICAL)
           return True
        else:
           base_url = 'https://www.cci.gov.in'
           Title = response.xpath('//*[@id="block-system-main"]/div/div/div[1]/table/tbody/tr/td[img[@class="file-icon"]]/ancestor::*[position()=1]/td/a/text()').extract()
           Links =  response.xpath('//*[@id="block-system-main"]/div/div/div[1]/table/tbody/tr/td[img[@class="file-icon"]]/ancestor::*[position()=1]/td/a/@href').extract()
         
           for item in zip(Title,Links):
                 scraped={
                        'Title':item[0],
                        'Link':base_url + item[1],
                        'BOT NAME':settings.get('BOT_NAME'),
                        'RUN TIME':settings.get('TODAYDATETEST')
                        }
                 yield scraped
           for urls in Links:
               Link = base_url + urls
               yield scrapy.Request(url=Link,callback=self.parse_p,dont_filter=True)
              
    def parse_p(self,response):
        path = response.xpath('//a[contains(.,"pdf")]/@href').extract()
        for paths in path:
            yield scrapy.Request(url=paths,callback=self.parsedownloadpdf,dont_filter=True)
  
        
            
    def parsedownloadpdf(self,response):
        path=response.url.split('/')[-1]        
        self.logger.info('Saving PDF downloadpdf %s', path)
        with open(path, 'wb') as f:           
            f.write(response.body)
            
  

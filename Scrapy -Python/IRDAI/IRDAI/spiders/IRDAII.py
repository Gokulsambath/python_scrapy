from scrapy.http import FormRequest
import scrapy
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()
import requests
from scrapy.utils.project import get_project_settings
import zipfile
import os
from scrapy import log
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import win32con, win32api
import requests
import datetime
from lxml import html
from dateutil.relativedelta import relativedelta
from scrapy.http import Request
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from apiclient import errors
from apiclient.http import MediaFileUpload
#from lxml import html

class quotesspider(scrapy.Spider):
    name = 'quotesspider'
    allowed_domains=['irdai.gov.in']
    start_urls = ['https://www.irdai.gov.in/ADMINCMS/cms/frmwhats_List.aspx']
    custom_settings = {
       'FEED_FORMAT' : 'csv',
       'FEED_URI' : 'IRDAI.csv'
    }
#    download_delay = 1.5
    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        
          
    def spider_closed(self, spider):
        settings = get_project_settings() 
#       params={"act":"",
#                "duedatefrom":settings.get('ENDDATETEST'),
#                "duedateto":settings.get('TODAYDATETEST')                 
#                }  
       
#       urlactual="https://www.irdai.gov.in/ADMINCMS/cms/frmwhats_List.aspx"
#       response = requests.get(urlactual)
#       doc = html.fromstring(response.text)

#       somedata1 = yield scrapy.Request(url=urlactual,callback=self.parse)
#
#       if not somedata1:
#            log.msg("This should never happen, XPath's are all wrong, OMG!", level=log.CRITICAL)
#       else:
        working_folder = settings.get('FOLDERPATH')
        a=0
        b=0
        for root, dirs, files in os.walk(working_folder):
            b=0
            for file in files:
                if file.endswith(".doc") or file.endswith(".pdf") or file.endswith(".xls") or file.endswith(".csv") or file.endswith(".zip"): 
                    a=1
                else:
                    b=1
        print(a)           
        if a >= 1 and not b > 1:            
            working_folder = settings.get('FOLDERPATH')
          
            files = os.listdir(working_folder)
            
            files_py = []
            
            for f in files:
                if f.endswith('.pdf') or f.endswith('.doc') or f.endswith('.xls'):
                    fff = os.path.join(working_folder, f)
                    files_py.append(fff)
            
            ZipFile = zipfile.ZipFile("IRDAI.zip", "w")
             
            for a in files_py:
                ZipFile.write(os.path.basename(a), compress_type=zipfile.ZIP_DEFLATED)
            ZipFile.close()
           
            creds = None
            SCOPES = ['https://www.googleapis.com/auth/drive']
            # The file token.pickle stores the user's access and refresh tokens, and is
            # created automatically when the authorization flow completes for the first
            # time.
            if os.path.exists('token.pickle'):
                with open('token.pickle', 'rb') as token:
                    creds = pickle.load(token)
            # If there are no (valid) credentials available, let the user log in.
            if not creds or not creds.valid:
                if creds and creds.expired and creds.refresh_token:
                    creds.refresh(Request())
                else:
                    flow = InstalledAppFlow.from_client_secrets_file(
                        'credentials.json', SCOPES)
                    creds = flow.run_local_server()
                # Save the credentials for the next run
                with open('token.pickle', 'wb') as token:
                    pickle.dump(creds, token)
                    
            service = build('drive', 'v3', credentials=creds, cache_discovery=False)
            TODAYDATESS=datetime.datetime.today() 
            TODAYDATETEST=TODAYDATESS.strftime('%Y-%m-%d')
            file_metadata = {
            'name': 'Scrapy [%s]' % TODAYDATETEST,
            'mimeType': 'application/vnd.google-apps.folder'
            }
            title = 'Scrapy [%s]' % TODAYDATETEST
            results = service.files().list(
                pageSize=10, fields="nextPageToken, files(id, name)").execute()
            items = results.get('files', [])
            a=0
            folder_id = 0
            if not items:
                print('No files found.')
            else:
                print('Files:')
                for item in items:
                    print(u'{0} ({1})'.format(item['name'], item['id']))
                    if item['name'] == title:
                       print("present")
                       a=item['id']                
                       break
                    else:
                        print("Not present")
                        a = 0
            if a==0:
                file = service.files().create(body=file_metadata,
                                                fields='id').execute()
                print('Folder ID: %s' % file.get('id'))
                folder_id = file.get('id')
            else:
                folder_id = a 
                # file = service.CreateFile(body = file_metadata 
                #                 )
            
                
            file = service.permissions().create(body={"role":"reader", "type":"anyone"}, fileId=folder_id).execute()
            file_name =['IRDAI.csv']
            for name in file_name:
                file_metadata = {
                'name': [name] ,
                'parents': [folder_id]
                }
                media = MediaFileUpload(name,
                                    mimetype='text/csv',
                                    resumable=True)
                file = service.files().create(body=file_metadata,
                                                media_body=media,
                                                fields='id').execute()
            file_name =['IRDAI.zip']
            for name in file_name:
                file_metadata = {
                'name': [name] ,
                'parents': [folder_id]
                }
                media = MediaFileUpload(name,
                                    mimetype='application/zip',
                                    resumable=True)
                file = service.files().create(body=file_metadata,
                                                media_body=media,
                                                fields='id').execute()
            print ('File ID: %s' % file.get('id'))


            file_name =['IRDAI.csv','IRDAI.zip']
            working_folder = settings.get('FOLDERPATH')
            files = os.listdir(working_folder)
            
            for f in files:
                if f.endswith('.pdf') or f.endswith('.xls') or f.endswith('.doc'):
                    win32api.SetFileAttributes(f, win32con.FILE_ATTRIBUTE_NORMAL)
                    os.remove(f)
            for file in file_name:
                    
                    win32api.SetFileAttributes(file, win32con.FILE_ATTRIBUTE_NORMAL)
                    os.remove(file)
            
          
           
                
     
    def parse(self, response):
       settings= get_project_settings()
       yield FormRequest.from_response(
                response,
                formdata={
                    '__EVENTTARGET':'',
                    '__EVENTARGUMENT':'',
                    '__VIEWSTATE': response.xpath('//*[@id="__VIEWSTATE"]//@value').extract(),
                    '__EVENTVALIDATION':response.xpath('//*[@id="__EVENTVALIDATION"]//@value').extract(),
                    'UCTops1:Txt_Search':'',
                    'Ddl_Year': settings.get('YEAR'),
                    'Ddl_Month':settings.get('MONTH_IN_NUMBER'),
                    'BtnGo':'GO',
                },
                callback=self.parse_results
            )


    def parse_results(self, response):
       settings= get_project_settings()
       somedata = response.xpath('//*[@id="TD1"]/table/tbody/tr[2]/td/table//td[1]/text()').extract()
       if not somedata:
           log.msg("This should never happen, XPath's are all wrong, OMG!", level=log.CRITICAL)
           return True
       else:
           Date = response.xpath('//*[@id="TD1"]/table/tbody/tr[2]/td/table//td[1]/text()').extract()
           Title = response.xpath('//*[@id="TD1"]/table/tbody/tr[2]/td/table//td[3]/text()').extract()
           urls = response.xpath('//*[@id="TD1"]/table/tbody/tr[2]/td/table//td[@class="links1"]/a/@onclick').extract()
           b=[]
           for sa in urls:
               a,d,c = sa.split("'")
               requesturl = 'https://www.irdai.gov.in/ADMINCMS/cms/whatsNew_Layout.aspx?page='+d+'&flag=1'
               b.append(requesturl)
           for urls in b:
               url = urls
               yield scrapy.Request(url,callback=self.parse_p)
               
               
           for item in zip(Date,Title,b):
                 scraped={
                        'Date':item[0],
                        'Title':item[1],                    
                        'urls':item[2],
                        'BOT NAME':settings.get('BOT_NAME'),
                        'RUN TIME':settings.get('TODAYDATETEST')
                        }
                 yield scraped
                 
             
    def parse_p(self,response):
        print(response.url)
        href=response.xpath('//a[contains(text(),"Download")]//@href').extract()
        global w
        for af in href:
            d,w,a,o,q =href[0].split("'")
        if not w:
            w=0
            pass
        else:
            if(w[-3:] == 'Pdf'):
                yield FormRequest(response.url,callback=self.parsedownloadpdf,method="POST",
                    formdata={
                   '__EVENTTARGET':w,
                   '__EVENTARGUMENT':'',
                   '__VIEWSTATE':response.xpath('//*[@id="__VIEWSTATE"]//@value').extract(),
                   '__EVENTVALIDATION':response.xpath('//*[@id="__EVENTVALIDATION"]//@value').extract()
                   },headers={
                            'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                            'Accept-Encoding':'gzip, deflate, br',
                            'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
                            'Content-Type':'application/x-www-form-urlencoded',
                            'Origin':'https://www.irdai.gov.in',
                            'Referer':'https://www.irdai.gov.in/ADMINCMS/cms/whatsNew_Layout.aspx?page=PageNo2818&flag=1',
                            'Upgrade-Insecure-Requests':'1',
                            'Connection':'keep-alive'
                            }
                    )
            elif(w[-3:] == 'xls'):
                  yield FormRequest(response.url,callback=self.parsedownloadxls,method="POST",
                    formdata={
                   '__EVENTTARGET':w,
                   '__EVENTARGUMENT':'',
                   '__VIEWSTATE':response.xpath('//*[@id="__VIEWSTATE"]//@value').extract(),
                   '__EVENTVALIDATION':response.xpath('//*[@id="__EVENTVALIDATION"]//@value').extract()
                   },headers={
                            'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                            'Accept-Encoding':'gzip, deflate, br',
                            'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
                            'Content-Type':'application/x-www-form-urlencoded',
                            'Origin':'https://www.irdai.gov.in',
                            'Referer':'https://www.irdai.gov.in/ADMINCMS/cms/whatsNew_Layout.aspx?page=PageNo2818&flag=1',
                            'Upgrade-Insecure-Requests':'1',
                            'Connection':'keep-alive'
                            }
                    )
            elif(w[-3:] == 'ord'):
                  yield FormRequest(response.url,callback=self.parsedownloadword,method="POST",
                    formdata={
                   '__EVENTTARGET':w,
                   '__EVENTARGUMENT':'',
                   '__VIEWSTATE':response.xpath('//*[@id="__VIEWSTATE"]//@value').extract(),
                   '__EVENTVALIDATION':response.xpath('//*[@id="__EVENTVALIDATION"]//@value').extract()
                   },headers={
                            'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                            'Accept-Encoding':'gzip, deflate, br',
                            'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
                            'Content-Type':'application/x-www-form-urlencoded',
                            'Origin':'https://www.irdai.gov.in',
                            'Referer':'https://www.irdai.gov.in/ADMINCMS/cms/whatsNew_Layout.aspx?page=PageNo2818&flag=1',
                            'Upgrade-Insecure-Requests':'1',
                            'Connection':'keep-alive'
                            }
                    )

            
    def parsedownloadpdf(self,response):
       
        path = response.url
        self.logger.info('Saving PDF downloadpdf %s', path)
        
        ok,name = path.split("PageNo")
        name,ok = name.split("&")
        pdf = '.pdf'
        name = name+pdf
        print(name)
        with open(name, 'wb') as f:
           
            f.write(response.body)
            
    def parsedownloadword(self,response):
       
        path = response.url
        self.logger.info('Saving WOrd downloadword %s', path)
        
        ok,name = path.split("PageNo")
        name,ok = name.split("&")
        doc = '.doc'
        name = name+doc
        print(name)
        with open(name, 'wb') as f:           
            f.write(response.body)
            
    def parsedownloadxls(self,response):
       
        path = response.url
        ok,name = path.split("PageNo")
        name,ok=name.split("&")
        xls = '.xls'
        name=name+xls
        print(name)
        self.logger.info('Saving Excel downloadxls %s', path)
        with open(name, 'wb') as f:
            
            f.write(response.body)

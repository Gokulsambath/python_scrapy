# -*- coding: utf-8 -*-

# Scrapy settings for biharlabour project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html
import datetime
from urllib.parse import urlencode
from dateutil.relativedelta import relativedelta

BOT_NAME = 'biharlabour'

SPIDER_MODULES = ['biharlabour.spiders']
NEWSPIDER_MODULE = 'biharlabour.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'biharlabour (+http://www.yourdomain.com)'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False
TODAYDATESS=datetime.datetime.today()              
# print(todaydatess)     
ONE_MONTH=TODAYDATESS - relativedelta(days=7) 
#Daily tracking recommended. Contains Many Updates On daily basis.if file size maybe large it won't be attached to mail. 
# print(one_month)     
TODAYDATETEST=TODAYDATESS.strftime('%d-%m-%Y') 
ENDDATETEST=ONE_MONTH.strftime('%d-%m-%Y')
VARIABLE = 'N'

FOLDERPATH='C:\\Scrapy -Python\\Labour Bihar\\biharlabour\\spiders'
FROM_EMAIL_UN='cspl.jobs.ricago@gmail.com'
FROM_EMAIL_PWD='cspl@1234'
CC= ['shailesh@ricago.com','badri@ricago.com','gautham@ricago.com']
FROM_EMAIL_SUBJECT='Todays Updates From Labour of Bihar!!'
FROM_EMAIL_BODY='Legal update from Bihar Labour Department'
TO_EMAIL_ID='asif@ricago.com'



# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'biharlabour.middlewares.BiharlabourSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'biharlabour.middlewares.BiharlabourDownloaderMiddleware': 543,
#}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
#ITEM_PIPELINES = {
#    'biharlabour.pipelines.BiharlabourPipeline': 300,
#}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

# -*- coding: utf-8 -*-
import scrapy
import zipfile
import os
from scrapy import log
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import win32con, win32api
from scrapy.utils.project import get_project_settings
from urllib.parse import urlencode
import datetime
from lxml import html
from dateutil.relativedelta import relativedelta
from scrapy.http import Request
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from apiclient import errors
from apiclient.http import MediaFileUpload


class HrylabourSpider(scrapy.Spider):
    name = 'hrylabour'
    allowed_domains = ['gov.in']
    start_urls = ['https://hrylabour.gov.in/home/readmore?cat_id=all&status=1']
    headers = {"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1(KHTML, like Gecko) CHrome/22.0.1207.1 Safari/537.1"}
    custom_settings = {
        'FEED_FORMAT' : 'csv',
        'FEED_URI': 'LabourHry.csv'
    }


    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self,spider):
        settings = get_project_settings()
        params = {
            "sdate": settings.get('ENDDATETEST'),
            "edate": settings.get('TODAYDATETEST'),
            "status": settings.get('STATUS')

        }
        urlactual = "https://hrylabour.gov.in/home/readmore?cat_id=all&keyword=&" +urlencode(params)
        working_folder = settings.get('FOLDERPATH')
        files = os.listdir(working_folder)
        files_py = []
        for f in files:
            if f.endswith('.PDF' or '.pdf' or '.jpeg' or '.JPEG'):
                fff = os.path.join(working_folder , f)
                files_py.append(fff)
        ZipFile = zipfile.ZipFile("LabourHry.zip","w")
        
        for a in files_py:
            ZipFile.write(os.path.basename(a),compress_type= zipfile.ZIP_DEFLATED)
        ZipFile.close()
        creds = None
        SCOPES = ['https://www.googleapis.com/auth/drive']
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)
                
        service = build('drive', 'v3', credentials=creds, cache_discovery=False)
        TODAYDATESS=datetime.datetime.today() 
        TODAYDATETEST=TODAYDATESS.strftime('%Y-%m-%d')
        file_metadata = {
        'name': 'Scrapy [%s]' % TODAYDATETEST,
        'mimeType': 'application/vnd.google-apps.folder'
        }
        title = 'Scrapy [%s]' % TODAYDATETEST
        results = service.files().list(
            pageSize=10, fields="nextPageToken, files(id, name)").execute()
        items = results.get('files', [])
        a=0
        folder_id = 0
        if not items:
            print('No files found.')
        else:
            print('Files:')
            for item in items:
                print(u'{0} ({1})'.format(item['name'], item['id']))
                if item['name'] == title:
                   print("present")
                   a=item['id']                
                   break
                else:
                    print("Not present")
                    a = 0
        if a==0:
            file = service.files().create(body=file_metadata,
                                            fields='id').execute()
            print('Folder ID: %s' % file.get('id'))
            folder_id = file.get('id')
        else:
            folder_id = a 
            # file = service.CreateFile(body = file_metadata 
            #                 )
        
            
        file = service.permissions().create(body={"role":"reader", "type":"anyone"}, fileId=folder_id).execute()
        file_name =['LabourHry.csv']
        for name in file_name:
            file_metadata = {
            'name': [name] ,
            'parents': [folder_id]
            }
            media = MediaFileUpload(name,
                                mimetype='text/csv',
                                resumable=True)
            file = service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()       
        file_name =['LabourHry.zip']
        for name in file_name:
            file_metadata = {
            'name': [name] ,
            'parents': [folder_id]
            }
            media = MediaFileUpload(name,
                                mimetype='application/zip',
                                resumable=True)
            file = service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()
        print ('File ID: %s' % file.get('id'))



        file_name = ['LabourHry.csv','LabourHry.zip']
        working_folder = settings.get('FOLDERPATH')
        files = os.listdir(working_folder)
        for f in files:
            if f.endswith('.PDF' or '.pdf' or '.jpeg' or '.JPEG'):
                win32api.SetFileAttributes(f,win32con.FILE_ATTRIBUTE_NORMAL)
                os.remove(f)
        for file in file_name:
            win32api.SetFileAttributes(file, win32con.FILE_ATTRIBUTE_NORMAL)
            os.remove(file)


    def parse(self, response):
        settings = get_project_settings()
        params = {
            "sdate": settings.get('ENDDATETEST'),
            "edate": settings.get('TODAYDATETEST'),
            "status": settings.get('STATUS')
        }
        urlactual = "https://hrylabour.gov.in/home/readmore?cat_id=all&keyword=&" +urlencode(params)
        yield scrapy.Request(url = urlactual , callback = self.parse_p)

    def parse_p(self,response):
        settings = get_project_settings()
        some_data = response.xpath('//div[@class="accordion-content unique"]//ul[@class="news"]//li//a[@target="_blank"]/text()').extract()
        if not some_data:
            log.msg("No data this should not happen",level = log.CRITICAL)
        else:
            for row in response.xpath('//div[@class="accordion-content unique"]//ul[@class="news"]//li'):
                yield {

                
                'Title': row.xpath('a[@target="_blank"]/text()').extract(),
                'Published By': row.xpath('span[2]/text()').extract(),
                'Issued Date': row.xpath('span[1]/text()').extract(),
                'Download Link': row.xpath('a/@href').extract(),
                'BotName':settings.get('BOT_NAME'),
                'Runtime':settings.get('TODAYDATESS')
                }
                for a in response.xpath('//div[@class="accordion-content unique"]//ul[@class="news"]//li//a/@href'):
                    link = a.extract();
                    if link.endswith('.PDF' or '.pdf' or '.jpeg' or '.JPEG'):
                        yield scrapy.Request(url = link,callback=self.save_pdf)
            next_page_url = response.xpath('//ul[@class="pagination pagination-sm no-margin pull-right"]//li//a[@rel="next"]/@href').extract_first()
            if next_page_url:
                next_page_url = response.urljoin(next_page_url)
                yield scrapy.Request(url = next_page_url , callback = self.parse_p)

    def save_pdf(self,response):
        path = response.url.split('/')[-1]
        self.logger.info('Saving PDF %s',path)
        with open(path,'wb') as f:
            f.write(response.body)              

                        




        

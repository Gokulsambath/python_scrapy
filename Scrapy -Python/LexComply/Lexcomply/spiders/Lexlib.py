
# -*- coding: utf-8 -*-
import scrapy
from urllib.parse import unquote
import zipfile
import os
from scrapy import log
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import win32con, win32api
from scrapy.utils.project import get_project_settings
from urllib.parse import urlencode
import requests 
from lxml import html
import datetime
from lxml import html
from dateutil.relativedelta import relativedelta
from scrapy.http import Request
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from apiclient import errors
from apiclient.http import MediaFileUpload

class LexcomplySpider(scrapy.Spider):
    name = 'lexcomply'
    allowed_domains = ['lexcomply.com']
    start_urls = ['https://lexcomply.com/lex-library/']
    custom_settings = {
       'FEED_FORMAT' : 'csv',
       'FEED_URI' : 'LexComply.csv'
    }
  
    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        
        
    def spider_closed(self, spider):
       settings = get_project_settings() 
       params={"act":"",
                "duedatefrom":settings.get('ENDDATETEST'),
                "duedateto":settings.get('TODAYDATETEST')                 
                }  
       
       urlactual="https://lexcomply.com/updates_display_web.php?" + urlencode(params)
       response = requests.get(urlactual)
       doc = html.fromstring(response.text)
#
       somedata1 = doc.xpath('//div[@id="doublescroll"]//table[@id="testTable"]//tbody//tr//text()')
      
       if not somedata1:
            log.msg("This should never happen, XPath's are all wrong, OMG!", level=log.CRITICAL)
       else:
            
        working_folder = settings.get('FOLDERPATH')
      
        files = os.listdir(working_folder)
        
        files_py = []
        
        for f in files:
            if f.endswith('.pdf'):
                fff = os.path.join(working_folder, f)
                files_py.append(fff)
        
        ZipFile = zipfile.ZipFile("LexComply.zip", "w")
         
        for a in files_py:
            ZipFile.write(os.path.basename(a), compress_type=zipfile.ZIP_DEFLATED)
        ZipFile.close()
        creds = None
        SCOPES = ['https://www.googleapis.com/auth/drive']
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)
                
        service = build('drive', 'v3', credentials=creds, cache_discovery=False)
        TODAYDATESS=datetime.datetime.today() 
        TODAYDATETEST=TODAYDATESS.strftime('%Y-%m-%d')
        file_metadata = {
        'name': 'Scrapy [%s]' % TODAYDATETEST,
        'mimeType': 'application/vnd.google-apps.folder'
        }
        title = 'Scrapy [%s]' % TODAYDATETEST
        results = service.files().list(
            pageSize=10, fields="nextPageToken, files(id, name)").execute()
        items = results.get('files', [])
        a=0
        folder_id = 0
        if not items:
            print('No files found.')
        else:
            print('Files:')
            for item in items:
                print(u'{0} ({1})'.format(item['name'], item['id']))
                if item['name'] == title:
                   print("present")
                   a=item['id']                
                   break
                else:
                    print("Not present")
                    a = 0
        if a==0:
            file = service.files().create(body=file_metadata,
                                            fields='id').execute()
            print('Folder ID: %s' % file.get('id'))
            folder_id = file.get('id')
        else:
            folder_id = a 
            # file = service.CreateFile(body = file_metadata 
            #                 )
        
            
        file = service.permissions().create(body={"role":"reader", "type":"anyone"}, fileId=folder_id).execute()
        file_name =['LexComply.csv']
        for name in file_name:
            file_metadata = {
            'name': [name] ,
            'parents': [folder_id]
            }
            media = MediaFileUpload(name,
                                mimetype='text/csv',
                                resumable=True)
            file = service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()
        file_name =['LexComply.zip']
        for name in file_name:
            file_metadata = {
            'name': [name] ,
            'parents': [folder_id]
            }
            media = MediaFileUpload(name,
                                mimetype='application/zip',
                                resumable=True)
            file = service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()
        print ('File ID: %s' % file.get('id'))

       
        file_name =['LexComply.zip']
        working_folder = settings.get('FOLDERPATH')
        files = os.listdir(working_folder)
        
        for f in files:
            if f.endswith('.pdf'):
                win32api.SetFileAttributes(f, win32con.FILE_ATTRIBUTE_NORMAL)
                os.remove(f)
        for file in file_name:
                
                win32api.SetFileAttributes(file, win32con.FILE_ATTRIBUTE_NORMAL)
                os.remove(file)
        
      
       
                
                
    def parse(self, response):
        settings = get_project_settings() 
        
        params={"act":"",
                "duedatefrom":settings.get('ENDDATETEST'),
                "duedateto":settings.get('TODAYDATETEST')                 
                }
        urlactual="https://lexcomply.com/updates_display_web.php?" + urlencode(params)
        yield scrapy.Request(url=urlactual, dont_filter=False, callback=self.parse_p)
       
       
        
    def parse_p(self,response):
        #response.url
       settings=get_project_settings()
       
       somedata = response.xpath('//div[@id="doublescroll"]//table[@id="testTable"]//tbody//tr//text()').extract()
      
       if not somedata:
            
            log.msg("This should never happen, XPath's are all wrong, OMG!", level=log.CRITICAL)
            
       else:
      
        Number=response.xpath('//div[@id="doublescroll"]//table[@id="testTable"]//tbody//tr//td[1]/text()').extract()
       
        Date=response.xpath('//div[@id="doublescroll"]//table[@id="testTable"]//tbody//tr//td[2]/text()').extract()
      
        Acts=response.xpath('//div[@id="doublescroll"]//table[@id="testTable"]//tbody//tr//td[3]/text()').extract()
      
        NewsTitle=response.xpath('//div[@id="doublescroll"]//table[@id="testTable"]//tbody//tr//td[4]/text()').extract()
        urls= response.xpath('//div[@id="doublescroll"]//table[@id="testTable"]//tbody//tr//td[5]/a[@href]/@href').extract()
#      
#        
        baseurl="https://lexcomply.com/";
        
        for item in zip(Number,Date,Acts,NewsTitle,urls):
             scraped={
                    'Sln No':item[0],
                    'Date':item[1],
                    'Acts':item[2],
                    'NewsTitle':item[3],
                    'urls':baseurl+item[4],
                    'BOT NAME':settings.get('BOT_NAME'),
                    'RUN TIME':settings.get('TODAYDATESS')
                    }
             yield scraped
             
       
            
        for url in urls:
            baseurl = 'https://lexcomply.com/'
            url = baseurl + url
            absolute_url = response.urljoin(url)
            yield scrapy.Request(absolute_url,callback=self.parse_hotel) 
            
        
            
            
    def parse_hotel(self, response):
        base_url = ''
         
        a =  response.xpath('//iframe[@id="frameID"]//@src[1]').extract()
        for link in a:
           if link.endswith('.pdf'):
             link = base_url + link
#             link1 = response.urlparse(link)
             data,link = link.split("url=")
             url = unquote(link)
             url =url.replace('+','%20')

#            
             self.logger.info(url)
             yield scrapy.Request(url, callback=self.save_pdf)
             
             

#        

    def save_pdf(self, response):
        path = response.url.split('/')[-1]
        self.logger.info('Saving PDF %s', path)
        with open(path, 'wb') as f:
            f.write(response.body)
       


import scrapy
import zipfile
import os
from scrapy import log
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import win32con, win32api
from scrapy.utils.project import get_project_settings
import datetime
from lxml import html
from dateutil.relativedelta import relativedelta
from scrapy.http import Request
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from apiclient import errors
from apiclient.http import MediaFileUpload


class DemoSpider(scrapy.Spider):
    name ="Demo"
    allowed_domains = ['tourism.gov.in']
    start_urls = ['http://tourism.gov.in/']
    headers = {"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1(KHTML, like Gecko) CHrome/22.0.1207.1 Safari/537.1"}
    custom_settings = {
        'FEED_FORMAT' : 'csv',
        'FEED_URI': 'tourism.csv'
    }

    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    
    def spider_closed(self,spider):
        settings = get_project_settings()
        working_folder = settings.get('FOLDERPATH')
        files = os.listdir(working_folder)
        files_py = []
        for f in files:
            if f.endswith('.pdf') or f.endswith('.zip'):
                fff = os.path.join(working_folder , f)
                files_py.append(fff)
        ZipFile = zipfile.ZipFile("tourism.zip", "w")
        for a in files_py:
            ZipFile.write(os.path.basename(a), compress_type = zipfile.ZIP_DEFLATED)
        ZipFile.close()
        creds = None
        SCOPES = ['https://www.googleapis.com/auth/drive']
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)
                
        service = build('drive', 'v3', credentials=creds, cache_discovery=False)
        TODAYDATESS=datetime.datetime.today() 
        TODAYDATETEST=TODAYDATESS.strftime('%Y-%m-%d')
        file_metadata = {
        'name': 'Scrapy [%s]' % TODAYDATETEST,
        'mimeType': 'application/vnd.google-apps.folder'
        }
        title = 'Scrapy [%s]' % TODAYDATETEST
        results = service.files().list(
            pageSize=10, fields="nextPageToken, files(id, name)").execute()
        items = results.get('files', [])
        a=0
        folder_id = 0
        if not items:
            print('No files found.')
        else:
            print('Files:')
            for item in items:
                print(u'{0} ({1})'.format(item['name'], item['id']))
                if item['name'] == title:
                   print("present")
                   a=item['id']                
                   break
                else:
                    print("Not present")
                    a = 0
        if a==0:
            file = service.files().create(body=file_metadata,
                                            fields='id').execute()
            print('Folder ID: %s' % file.get('id'))
            folder_id = file.get('id')
        else:
            folder_id = a 
            # file = service.CreateFile(body = file_metadata 
            #                 )
        
            
        file = service.permissions().create(body={"role":"reader", "type":"anyone"}, fileId=folder_id).execute()
        file_name =['tourism.csv']
        for name in file_name:
            file_metadata = {
            'name': [name] ,
            'parents': [folder_id]
            }
            media = MediaFileUpload(name,
                                mimetype='text/csv',
                                resumable=True)
            file = service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()
        file_name =['tourism.zip']
        for name in file_name:
            file_metadata = {
            'name': [name] ,
            'parents': [folder_id]
            }
            media = MediaFileUpload(name,
                                mimetype='application/zip',
                                resumable=True)
            file = service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()
        print ('File ID: %s' % file.get('id'))

        file_name = ['tourism.zip']
        working_folder  =settings.get('FOLDERPATH')
        files = os.listdir(working_folder)
       
        for f in files:
            if f.endswith('.pdf') or f.endswith('.csv'):
                win32api.SetFileAttributes(f,win32con.FILE_ATTRIBUTE_NORMAL)
                os.remove(f)
        for file in file_name:
            win32api.SetFileAttributes(file,win32con.FILE_ATTRIBUTE_NORMAL)
            os.remove(file)

    

    def parse(self,response):
        settings = get_project_settings()  
        somedata = response.xpath('//div[@id="block-views-ministry-updates-block"]//div[@class="view-content"]//ul[@class="ministry-block"]//li//a/text()').extract()
        print(somedata)
        if not somedata:
            log.msg("No data. This should not happen", level = log.CRITICAL)
        else:
            update = response.xpath('//div[@id="block-views-ministry-updates-block"]//div[@class="view-content"]//a')
            for row in update:
               Title =  row.xpath('text()').extract()                    
               linkss = row.xpath('@href').extract()
               base_url = 'http://www.tourism.gov.in'                  
               for item in zip(Title,linkss):
                        scraped = {                                
                                'Title':item[0],
                                'Links':base_url + item[1],
                                'BOT NAME':settings.get('BOT_NAME'),
                                'RUN TIME':settings.get('TODAYDATESS')
                                }
                        yield scraped
               for link in linkss:
                        base_url = 'http://www.tourism.gov.in'
                        link = base_url + link
                        yield scrapy.Request(url=link,callback=self.parse_p,dont_filter=True)

            
    def parse_p(self,response):
        Links = response.xpath('//a[contains(@href,".pdf")]/@href').extract_first()
        if Links.endswith('.pdf'):
                yield scrapy.Request(url =Links,callback = self.save_pdf,dont_filter=True)              


    def save_pdf(self,response):
        name = response.url.split('/')[-1]
        self.logger.info('Saving PDF %s',name)
        with open(name,'wb') as f:
            f.write(response.body)
    

# -*- coding: utf-8 -*-
import scrapy
import zipfile
import os
from scrapy import signals
import datetime
from scrapy.xlib.pydispatch import dispatcher
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import win32con, win32api
import time
from dateutil.parser import parse
from scrapy.utils.project import get_project_settings
from urllib.parse import urlencode
import datetime
from lxml import html
from dateutil.relativedelta import relativedelta
from scrapy.http import Request
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from apiclient import errors
from apiclient.http import MediaFileUpload  




class DemosSpider(scrapy.Spider):
    name = 'demos'
    allowed_domains = ['nseindia.com']
    start_urls = ['https://www.nseindia.com/circulars/content/circ_latest.htm']
    headers = {"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1"}
    custom_settings = {
        'FEED_FORMAT' : 'csv',
        'FEED_URI' : 'MinistryNSE.csv'
    }

    def __init__(self):
        dispatcher.connect(self.spider_closed,signals.spider_closed)

    def spider_closed(self,spider):
        settings = get_project_settings()
        working_folder = settings.get('FOLDERPATH')
        files = os.listdir(working_folder)
        files_py = []
        for f in files:
            if f.endswith('.pdf') or f.endswith('.zip'):
                fff = os.path.join(working_folder , f)
                files_py.append(fff)
        ZipFile = zipfile.ZipFile("MinistryNSE.zip", "w")
        for a in files_py:
            ZipFile.write(os.path.basename(a),compress_type = zipfile.ZIP_DEFLATED)
        ZipFile.close()
        creds = None
        SCOPES = ['https://www.googleapis.com/auth/drive']
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server()
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)
                
        service = build('drive', 'v3', credentials=creds, cache_discovery=False)
        TODAYDATESS=datetime.datetime.today() 
        TODAYDATETEST=TODAYDATESS.strftime('%Y-%m-%d')
        file_metadata = {
        'name': 'Scrapy [%s]' % TODAYDATETEST,
        'mimeType': 'application/vnd.google-apps.folder'
        }
        title = 'Scrapy [%s]' % TODAYDATETEST
        results = service.files().list(
            pageSize=10, fields="nextPageToken, files(id, name)").execute()
        items = results.get('files', [])
        a=0
        folder_id = 0
        if not items:
            print('No files found.')
        else:
            print('Files:')
            for item in items:
                print(u'{0} ({1})'.format(item['name'], item['id']))
                if item['name'] == title:
                   print("present")
                   a=item['id']                
                   break
                else:
                    print("Not present")
                    a = 0
        if a==0:
            file = service.files().create(body=file_metadata,
                                            fields='id').execute()
            print('Folder ID: %s' % file.get('id'))
            folder_id = file.get('id')
        else:
            folder_id = a 
            # file = service.CreateFile(body = file_metadata 
            #                 )
        
            
        file = service.permissions().create(body={"role":"reader", "type":"anyone"}, fileId=folder_id).execute()
        file_name =['MinistryNSE.csv']
        for name in file_name:
            file_metadata = {
            'name': [name] ,
            'parents': [folder_id]
            }
            media = MediaFileUpload(name,
                                mimetype='text/csv',
                                resumable=True)
            file = service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()
        file_name =['MinistryNSE.zip']
        for name in file_name:
            file_metadata = {
            'name': [name] ,
            'parents': [folder_id]
            }
            media = MediaFileUpload(name,
                                mimetype='application/zip',
                                resumable=True)
            file = service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()
        print ('File ID: %s' % file.get('id'))

        file_name = ['MinistryNSE.csv','MinistryNSE.zip']
        working_folder = settings.get('FOLDERPATH')
        files = os.listdir(working_folder)
        for f in files:
            if f.endswith('.pdf'):
                win32api.SetFileAttributes(f,win32con.FILE_ATTRIBUTE_NORMAL)
                os.remove(f)
        for file in file_name:
            win32api.SetFileAttributes(file,win32con.FILE_ATTRIBUTE_NORMAL)
            os.remove(file)

    def parse(self,response):
        settings = get_project_settings()
        urlactual = "https://www.nseindia.com/circulars/content/circ_latest.htm"
        yield scrapy.Request(url = urlactual,callback = self.parse_p)

    def parse_p(self,response):
        settings = get_project_settings()
        year = settings.get('ENDDATETEST')
        print("Files are to be downloaded from " +year)
        convertyear = time.strptime(year,'%d/%m/%Y')
        for row in response.xpath('//table//tr'):
            Date = row.xpath('td[1]//text()').extract()
            Subject = row.xpath('td[2]//a[@target="new"]//text()').extract()
            Link = row.xpath('td[2]//a[@target="new"]//@href').extract()
            Category = row.xpath('td[3]//text()').extract()
            CircularNo = row.xpath('td[4]//text()').extract()
            for ab in Link:
                baseurl = "https://www.nseindia.com"
                downloadlink = baseurl + ab
                print(downloadlink)
                for dates in Date:
                    ab = datetime.datetime.strptime(dates,'%B     %d, %Y').strftime('%d/%m/%Y')
                    ac = time.strptime(ab,'%d/%m/%Y')
                    print(ac)
                    print(convertyear)
                    if(ac>=convertyear):
                        print("true")
                        yield{
                            'Date': Date,
                            'Subject' : Subject,
                            'Download Link': downloadlink,
                            'Category': Category,
                            'CirularNo': CircularNo,
                            'Botname' : settings.get('BOT_NAME'),
                            'Runtime' : settings.get('TODAYDATE')
                        }
                        if downloadlink.endswith('.pdf'):
                            yield scrapy.Request(url = downloadlink,callback = self.save_pdf)
                    else:
                        print('No files to download from the given date')
                        continue

    def save_pdf(self,response):
        path = response.url.split('/')[-1]
        self.logger.info('Saving PDF %s',path)
        with open(path,'wb') as f:
            f.write(response.body)                  



        

        

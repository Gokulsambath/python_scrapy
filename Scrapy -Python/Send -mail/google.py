from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from apiclient import errors
from apiclient.http import MediaFileUpload
import datetime
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import requests
import win32con, win32api

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/drive']

def main():
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
            
    service = build('drive', 'v3', credentials=creds)
    TODAYDATESS=datetime.datetime.today() 
    TODAYDATETEST=TODAYDATESS.strftime('%Y-%m-%d')
#    file_metadata = {
#    'name': 'Scrapy [%s]' % TODAYDATETEST,
#    'mimeType': 'application/vnd.google-apps.folder'
#    }
    title = 'Scrapy [%s]' % TODAYDATETEST
    results = service.files().list(
        pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])
    a=0
    folder_id = 0
    if not items:
        print('No files found.')
    else:
        print('Files:')
        for item in items:
            print(u'{0} ({1})'.format(item['name'], item['id']))
            if item['name'] == title:
                print("present")
                a=item['id']                
                break
            else:
                print("Not present")
                a = 0
    if a==0:
        pass
#        file = service.files().create(body=file_metadata,
#                                        fields='id').execute()
#        print('Folder ID: %s' % file.get('id'))
#        folder_id = file.get('id')
    else:
        folder_id = a 
    # file = service.CreateFile(body = file_metadata 
    #                 )

    
    file = service.permissions().create(body={"role":"reader", "type":"anyone"}, fileId=folder_id).execute()
#    FOLDERPATH='C:\Scrapy -Python\IRDAI\IRDAI\spiders'
#    file_name =['IRDAI.csv']
#    for name in file_name:
#        file_metadata = {
#        'name': [name] ,
#        'parents': [folder_id]
#        }
#        media = MediaFileUpload(name,
#                            mimetype='application/zip',
#                            resumable=True)
#        file = service.files().create(body=file_metadata,
#                                        media_body=media,
#                                        fields='id').execute()
    print ('File ID: %s' % file.get('id'))
    Link = 'https://drive.google.com/drive/folders/'+folder_id+'?usp=sharing'
    subject = 'Updates From ScrapyBot'
    CC =['shailesh@ricago.com','gautham@ricago.com','badri@ricago.com']
    #['arunkumar@ricago.com','amogh@ricago.com']
    email_send = 'asif@ricago.com'
    msg = MIMEMultipart()
    msg['From'] = 'cspl.jobs.ricago@gmail.com'
    msg['To'] = email_send
    msg['Subject'] = subject
    email_user =  'cspl.jobs.ricago@gmail.com'
    email_password = 'cspl@1234'
    msg['Cc'] = ",".join(CC)
    body =  "Your Today's Update is Uploaded in Google Drive and the Link To Access is given Below " + Link
    msg.attach(MIMEText(body,'plain'))
    
    email_send = [email_send] + CC
    text = msg.as_string()
    server = smtplib.SMTP('smtp.gmail.com',587)
    server.starttls()
    server.login(email_user,email_password)
        
        
    server.sendmail(email_user,email_send,text)
    server.quit()




    # Call the Drive v3 API
    results = service.files().list(
        pageSize=10, fields="nextPageToken, files(id, name)").execute()
    items = results.get('files', [])

    if not items:
        print('No files found.')
    else:
        print('Files:')
        for item in items:
            print(u'{0} ({1})'.format(item['name'], item['id']))

if __name__ == '__main__':
    main()

# def remove_duplicates(dir):
#     unique = []
#     for filename in os.listdir(dir):
#         if os.path.isfile(filename):
#             filehash = md5.md5(file(filename).read()).hexdigest()
#         if filehash not in unique: 
#             unique.append(filehash)
#         else: 
#             os.remove(filename)

